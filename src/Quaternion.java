import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Quaternions. Basic operations.
 */
public class Quaternion {

    final static double epsilon = 0.0000001;

    private double a;
    private double i;
    private double j;
    private double k;

    /**
     * Constructor from four double values.
     *
     * @param a real part
     * @param b imaginary part i
     * @param c imaginary part j
     * @param d imaginary part k
     */
    public Quaternion(double a, double b, double c, double d) {
        this.a = a;
        this.i = b;
        this.j = c;
        this.k = d;
    }

    /**
     * Real part of the quaternion.
     *
     * @return real part
     */
    public double getRpart() {

        return a;
    }

    /**
     * Imaginary part i of the quaternion.
     *
     * @return imaginary part i
     */
    public double getIpart() {
        return i;
    }

    /**
     * Imaginary part j of the quaternion.
     *
     * @return imaginary part j
     */
    public double getJpart() {
        return j;
    }

    /**
     * Imaginary part k of the quaternion.
     *
     * @return imaginary part k
     */
    public double getKpart() {
        return k;
    }

    /**
     * Conversion of the quaternion to the string.
     *
     * @return a string form of this quaternion:
     * "a+bi+cj+dk"
     * (without any brackets)
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(a);

        if (i != 0 || (j != 0 || k != 0)) {
            if (i < 0) {
                sb.append(i);
            } else {
                sb.append("+").append(i);
            }
            sb.append("i");
        }

        if (j != 0 || k != 0) {
            if (j < 0) {
                sb.append(j);
            } else {
                sb.append("+").append(j);
            }
            sb.append("j");
        }

        if (k != 0) {
            if (k < 0) {
                sb.append(k);
            } else {
                sb.append("+").append(k);
            }
            sb.append("k");
        }
        // sb.append(a).append("+").append(i).append("i").append("+").append(j).append("j").append("+").append(k).append("k");
        return sb.toString();
    }

    public static void main(String[] args) {
        Quaternion f = new Quaternion(3., 0., 0., -1.);
        //System.out.println(f.toString());
        System.out.println(valueOf(f.toString()));
    }

    /**
     * Conversion from the string to the quaternion.
     * Reverse to <code>toString</code> method.
     *
     * @param s string of form produced by the <code>toString</code> method
     * @return a quaternion represented by string s
     * @throws IllegalArgumentException if string s does not represent
     *                                  a quaternion (defined by the <code>toString</code> method)
     */
    public static Quaternion valueOf(String s) {
        int sLength = s.length();
        if (s.equals("")) {
            throw new RuntimeException("Empty string inputted");
        }

        double a, b, c, d;
        //s = s.replace("i", "").replace("j", "").replace("k", "");
        s = s.trim();
        s += " ";
        List<String> allMatches = new ArrayList<>();
        // ([+-]?\d+([.]\d+)?\w)
        // ([+-]?\d+([.]\d+)?)([+-]?\d+([.]\d+)?[i])?([+-]?\d+([.]\d+)?[j])?([+-]?\d+([.]\d+)?[k])?
        Matcher m = Pattern.compile("([+-]?\\d+([.]\\d+)?[ijk])?")
                .matcher(s);
        Matcher m2 = Pattern.compile("([+-]?\\d+([.]\\d+)?([+-]|\\s))")
                .matcher(s);

        while (m2.find()) {
            if (!m2.group().equals("")) {
                allMatches.add(m2.group().substring(0, m2.group().length() - 1));
            }
        }
        if (allMatches.size() > 1) {
            throw new RuntimeException("Too many r values in " + s);
        }
        while (m.find()) {
            if (!m.group().equals("")) {
                allMatches.add(m.group());
            }
        }
        int allMatchesLength = 0;
        for (String match :
                allMatches) {
            allMatchesLength += match.length();
        }
        if (sLength > allMatchesLength) {
            throw new RuntimeException("Too many unnecessary symbols/characters in: " + s);
        }

        List<String> elements = new ArrayList<>();
        if (allMatches.size() > 1) {
            if (allMatches.get(1).contains("i") && containsOnce(s, "i")) {
                elements.add("i");
                b = Double.parseDouble(allMatches.get(1).replace("i", ""));
            } else {
                throw new RuntimeException("Only one 'i' allowed in " + s);
            }
        } else {
            b = 0;
        }
        if (allMatches.size() > 2) {
            if (!elements.contains("i")) {
                throw new RuntimeException("No 'i' found in " + s);
            }
            if (allMatches.get(2).contains("j") && containsOnce(s, "j")) {
                elements.add("j");
                c = Double.parseDouble(allMatches.get(2).replace("j", ""));
            } else {
                throw new RuntimeException("Only one 'j' allowed in " + s);
            }
        } else {
            c = 0;
        }
        if (allMatches.size() > 3) {
            if (!elements.contains("j")) {
                throw new RuntimeException("No 'j' found in "+s);
            } else if (!elements.contains("i")) {
                throw new RuntimeException("No 'i' found in "+s);
            }
            if (allMatches.get(3).contains("k") && containsOnce(s, "k")) {
                d = Double.parseDouble(allMatches.get(3).replace("k", ""));
            } else {
                throw new RuntimeException("Only one 'k' allowed in " + s);
            }
        } else {
            d = 0;
        }
        a = Double.parseDouble(allMatches.get(0));

        return new Quaternion(a, b, c, d);
    }

    public static boolean containsOnce(final String s, final CharSequence substring) {
        Pattern pattern = Pattern.compile(substring.toString());
        Matcher matcher = pattern.matcher(s);
        if (matcher.find()) {
            return !matcher.find();
        }
        return false;
    }

    /**
     * Clone of the quaternion.
     *
     * @return independent clone of <code>this</code>
     */
    @Override
    public Object clone() throws CloneNotSupportedException {

        return new Quaternion(a, i, j, k);
    }

    /**
     * Test whether the quaternion is zero.
     *
     * @return true, if the real part and all the imaginary parts are (close to) zero
     */
    public boolean isZero() {
        return a >= -epsilon && a <= epsilon &&
                i >= -epsilon && i <= epsilon &&
                j >= -epsilon && j <= epsilon &&
                k >= -epsilon && k <= epsilon;
    }

    /**
     * Conjugate of the quaternion. Expressed by the formula
     * conjugate(a+bi+cj+dk) = a-bi-cj-dk
     *
     * @return conjugate of <code>this</code>
     */
    public Quaternion conjugate() {
        return new Quaternion(a, -i, -j, -k);
    }

    /**
     * Opposite of the quaternion. Expressed by the formula
     * opposite(a+bi+cj+dk) = -a-bi-cj-dk
     *
     * @return quaternion <code>-this</code>
     */
    public Quaternion opposite() {
        return new Quaternion(-a, -i, -j, -k);
    }

    /**
     * Sum of quaternions. Expressed by the formula
     * (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
     *
     * @param q addend
     * @return quaternion <code>this+q</code>
     */
    public Quaternion plus(Quaternion q) {
        return new Quaternion(a + q.getRpart(), i + q.getIpart(), j + q.getJpart(), k + q.getKpart());
    }

    /**
     * Product of quaternions. Expressed by the formula
     * (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) =
     * (a1a2-b1b2-c1c2-d1d2) +
     * (a1b2+b1a2+c1d2-d1c2)i +
     * (a1c2-b1d2+c1a2+d1b2)j +
     * (a1d2+b1c2-c1b2+d1a2)k
     *
     * @param q factor
     * @return quaternion <code>this*q</code>
     */
    public Quaternion times(Quaternion q) {
        return new Quaternion(
                a * q.getRpart() - i * q.getIpart() - j * q.getJpart() - k * q.getKpart(),
                a * q.getIpart() + i * q.getRpart() + j * q.getKpart() - k * q.getJpart(),
                a * q.getJpart() - i * q.getKpart() + j * q.getRpart() + k * q.getIpart(),
                a * q.getKpart() + i * q.getJpart() - j * q.getIpart() + k * q.getRpart());
    }

    /**
     * Multiplication by a coefficient.
     *
     * @param r coefficient
     * @return quaternion <code>this*r</code>
     */
    public Quaternion times(double r) {
        return new Quaternion(a * r, i * r, j * r, k * r);
    }

    /**
     * Inverse of the quaternion. Expressed by the formula
     * 1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) +
     * ((-b)/(a*a+b*b+c*c+d*d))i +
     * ((-c)/(a*a+b*b+c*c+d*d))j +
     * ((-d)/(a*a+b*b+c*c+d*d))k
     *
     * @return quaternion <code>1/this</code>
     */
    public Quaternion inverse() {
        if (isZero()) {
            throw new RuntimeException("Can't inverse a zero(isZero()=true) Quaternion");
        }
        return new Quaternion(a / (a * a + i * i + j * j + k * k),
                -i / (a * a + i * i + j * j + k * k),
                -j / (a * a + i * i + j * j + k * k),
                -k / (a * a + i * i + j * j + k * k));
    }

    /**
     * Difference of quaternions. Expressed as addition to the opposite.
     *
     * @param q subtrahend
     * @return quaternion <code>this-q</code>
     */
    public Quaternion minus(Quaternion q) {
        return new Quaternion(a - q.getRpart(), i - q.getIpart(), j - q.getJpart(), k - q.getKpart());
    }

    /**
     * Right quotient of quaternions. Expressed as multiplication to the inverse.
     *
     * @param q (right) divisor
     * @return quaternion <code>this*inverse(q)</code>
     */
    public Quaternion divideByRight(Quaternion q) {
        if (q.isZero()) {
            throw new RuntimeException("Can't divide by right a zero(isZero()=true) Quaternion");
        }
        return times(q.inverse());
    }

    /**
     * Left quotient of quaternions.
     *
     * @param q (left) divisor
     * @return quaternion <code>inverse(q)*this</code>
     */
    public Quaternion divideByLeft(Quaternion q) {
        if (q.isZero()) {
            throw new RuntimeException("Can't divide by left a zero(isZero()=true) Quaternion");
        }
        return q.inverse().times(this);
    }

    /**
     * Equality test of quaternions. Difference of equal numbers
     * is (close to) zero.
     *
     * @param qo second quaternion
     * @return logical value of the expression <code>this.equals(qo)</code>
     */
    @Override
    public boolean equals(Object qo) {

        Quaternion qoo = (Quaternion) qo;
        return a - qoo.a < epsilon && i - qoo.i < epsilon &&
                j - qoo.j < epsilon && k - qoo.k < epsilon;
    }

    /**
     * Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
     *
     * @param q factor
     * @return dot product of this and q
     */
    public Quaternion dotMult(Quaternion q) {
        Quaternion aQuat = this;

        times(q.conjugate());
        plus(q.times(aQuat.conjugate()));

        return new Quaternion(times(0.5).getRpart(), 0, 0, 0);
    }

    /**
     * Integer hashCode has to be the same for equal objects.
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(a, i, k, j);
    }

    /**
     * Norm of the quaternion. Expressed by the formula
     * norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
     *
     * @return norm of <code>this</code> (norm is a real number)
     */
    public double norm() {
        return Math.sqrt(a * a + i * i + j * j + k * k);
    }

    /**
     * Main method for testing purposes.
     *
     * @param arg command line parameters
     */
    public static void main2(String[] arg) {
        Quaternion arv1 = new Quaternion(-1., 1, 2., -2.);
        if (arg.length > 0)
            arv1 = valueOf(arg[0]);
        System.out.println("first: " + arv1.toString());
        System.out.println("real: " + arv1.getRpart());
        System.out.println("imagi: " + arv1.getIpart());
        System.out.println("imagj: " + arv1.getJpart());
        System.out.println("imagk: " + arv1.getKpart());
        System.out.println("isZero: " + arv1.isZero());
        System.out.println("conjugate: " + arv1.conjugate());
        System.out.println("opposite: " + arv1.opposite());
        System.out.println("hashCode: " + arv1.hashCode());
        Quaternion res = null;
        try {
            res = (Quaternion) arv1.clone();
        } catch (CloneNotSupportedException e) {
        }
        ;
        System.out.println("clone equals to original: " + res.equals(arv1));
        System.out.println("clone is not the same object: " + (res != arv1));
        System.out.println("hashCode: " + res.hashCode());
        res = valueOf(arv1.toString());
        System.out.println("string conversion equals to original: "
                + res.equals(arv1));
        Quaternion arv2 = new Quaternion(1., -2., -1., 2.);
        if (arg.length > 1)
            arv2 = valueOf(arg[1]);
        System.out.println("second: " + arv2.toString());
        System.out.println("hashCode: " + arv2.hashCode());
        System.out.println("equals: " + arv1.equals(arv2));
        res = arv1.plus(arv2);
        System.out.println("plus: " + res);
        System.out.println("times: " + arv1.times(arv2));
        System.out.println("minus: " + arv1.minus(arv2));
        double mm = arv1.norm();
        System.out.println("norm: " + mm);
        System.out.println("inverse: " + arv1.inverse());
        System.out.println("divideByRight: " + arv1.divideByRight(arv2));
        System.out.println("divideByLeft: " + arv1.divideByLeft(arv2));
        System.out.println("dotMult: " + arv1.dotMult(arv2));
    }
}
// end of file
